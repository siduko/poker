package me.dev.models;

import me.dev.common.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class BanCo implements IBanCo{
	private List<NguoiChoi> nguoiChois;

	private BoBai luotBaiTruoc;
	
	private BoBai boBai;
	
	public BanCo() {
		super();
		khoiTaoBanCo();
	}
	
	public BanCo(List<NguoiChoi> nguoiChois, BoBai boBai) {
		super();
		this.nguoiChois = nguoiChois;
		this.boBai = boBai;
	}
	
	public List<NguoiChoi> getNguoiChois() {
		return nguoiChois;
	}

	public void setNguoiChois(List<NguoiChoi> nguoiChois) {
		this.nguoiChois = nguoiChois;
	}

	public BoBai getBoBai() {
		return boBai;
	}

	public List<Bai> getBais(){
		return boBai.getBais();
	}

	public void setBoBai(BoBai boBai) {
		this.boBai = boBai;
	}

	@Override
	public void themNguoiChoi(NguoiChoi nguoiChoi) {
		this.getNguoiChois().add(nguoiChoi);
	}

	//tạo bộ bài sẵn 52 cây
	public void khoiTaoBanCo() {
		boBai = new BoBai();
		for (SoBai soBai : SoBai.values()) {
			for (ChatBai chatBai : ChatBai.values()) {
				Bai bai = new Bai(soBai, chatBai);
				boBai.themBai(bai);
			}
		}
		nguoiChois = new ArrayList<NguoiChoi>();
		luotBaiTruoc = new BoBai();
	}

	//xáo bộ bài
	public void xaoBai() {
		Collections.shuffle(this.boBai.getBais());
	}

	//chia bài cho các người chơi trong bàn cờ
	public void chiaBai() {
		for (int i = 0; i < Constants.SO_LA_BAI_CHO_NGUOI_CHOI; i++) {
			for (NguoiChoi nguoiChoi : nguoiChois) {
				Bai bai = this.boBai.layBai();
				nguoiChoi.themBai(bai);
			}
		}
		for (NguoiChoi nguoiChoi : nguoiChois){
			nguoiChoi.getBoBai().xepBai();
		}
	}

	//xáo vào chia bài cho các người chơi trong bàn cờ
	public void xaoVaChiaBai() {
		xaoBai();
		chiaBai();
	}

	@Override
	public boolean danhBai(NguoiChoi nguoiChoi, List<Bai> bais) {
		//danh luot dau
		if(luotBaiTruoc.getBais().size()==0){
			nguoiChoi.getBoBai().l
		}else{

		}
		return false;
	}

}
