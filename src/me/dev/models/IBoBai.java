package me.dev.models;

public interface IBoBai {
	void themBai(Bai baiKhac);
	void xoaBai(Bai baiKhac);
	void xepBai();
	void layBai(BoBai bobai);
}
