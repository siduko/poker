package me.dev.models;

import java.util.Comparator;

public class Bai implements Comparator<Bai>{
	private SoBai soBai;
	private ChatBai chatBai;
	public Bai() {
		super();
	}
	public Bai(SoBai soBai, ChatBai chatBai) {
		super();
		this.soBai = soBai;
		this.chatBai = chatBai;
	}
	public SoBai getSoBai() {
		return soBai;
	}
	public void setSoBai(SoBai soBai) {
		this.soBai = soBai;
	}
	public ChatBai getChatBai() {
		return chatBai;
	}
	public void setChatBai(ChatBai chatBai) {
		this.chatBai = chatBai;
	}
	
	public int compare(Bai bai1, Bai bai2) {
		int doLechSo = bai1.soBai.compareTo(bai2.soBai);
		if(doLechSo>0)
		{
			return 1;
		}else if(doLechSo==0){
			int doLechChat = bai1.chatBai.compareTo(bai2.chatBai);
			if(doLechChat>0)
				return 1;
			else
				return -1;
		}
		return -1;
	}
	
	
}
