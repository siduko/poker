package me.dev.models;

import java.util.ArrayList;
import java.util.List;

public class BoBai implements IBoBai {
	
	private List<Bai> bais;
	
	public BoBai() {
		bais = new ArrayList<Bai>();
	}
	
	public BoBai(List<Bai> bais) {
		super();
		this.bais = bais;
	}

	public List<Bai> getBais() {
		return bais;
	}

	public void setBais(List<Bai> bais) {
		this.bais = bais;
	}
	
	public boolean soSanh(BoBai boBai){
		
		return false;
	}

	public void xepBai() {
		// TODO Auto-generated method stub
		bais.sort(new Bai());
	}

	public void layBai(BoBai bobai) {
		// TODO Auto-generated method stub
		List<Bai> baiKhacs = bobai.bais;
		for(int i = 0 ; i < baiKhacs.size() ; i ++) {
			if(baiKhacs.get(i).compare(baiKhacs.get(i), bais.get(i)) == 0) {
				xoaBai(bais.get(i));
			}
		}
	}

	public void themBai(Bai baiKhac) {
		// TODO Auto-generated method stub
		bais.add(baiKhac);
	}

	public void xoaBai(Bai baiKhac) {
		// TODO Auto-generated method stub
		for(int i = 0 ; i < bais.size() ; i ++){
			if(bais.get(i).compare(bais.get(i), baiKhac) == 0){
				bais.remove(i);
			}
		}
	}
	
}
